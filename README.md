
# NTUPLE ANALYZER

Analysis from flat n-tuples.

Since the used rootfile is placed in a public folder, running over this test sample should be possible for everyone on LXPLUS.

## Adaption to user

Before running the code, the path stored in the variable "TString work_dir_path" of "Main.C" has to be adapted to your personal workspace.
Additionally there are paths within the TSelector classe (hepper/DataSelector.C), that have to be either
adapted to the user, or rewritten in a way that interpreted ROOT finds the include paths by itself.

Also the "path" variable in the filesList.sh script has to point to where you store the root files that you want to analyse.

### Execution

To run this code, do:

```
source setup.sh
source filesList.sh
source execute.sh true
```
