#!/usr/bin/env python
import os, sys
import subprocess

def main():
    rfiles = os.listdir('output')
    files = ''
    for rf in rfiles:
       if rf.endswith('.root'):
         files += ' output/' + rf 
    cmd = 'hadd merged.root' + ' ' + files
    print cmd
    subprocess.call(cmd, shell=True)


if __name__ == "__main__":
   main()
