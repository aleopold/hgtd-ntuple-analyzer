
#include "Track.h"
ClassImp(Track)

double Track::pt() {
  return m_pt;
}

double Track::e() {
  return m_energy;
}

double Track::eta() {
  return m_eta;
}

double Track::phi() {
  return m_phi;
}

double Track::z0() {
  return m_z0;
}

bool Track::isTruth() {
  return m_truth_matched;
}

TVector3 extrapolate(const int& layer) {
  switch (layer) {
        case 0:
          return l0;
        case 1:
          return l1;
        case 2:
          return l2;
        case 3:
          return l3;
        default:
          std::cout<<"Track::extrapolate: invalid layer value, returning empty TVector3." << std::endl;
          TVector3 empty;
          return empty;
    }
}

void Track::setPt(const double& pt) {
  m_pt = pt;
}

void Track::setE(const double& e) {
  m_energy = e;
}

void Track::setEta(const double& eta) {
  m_eta = eta;
}

void Track::setPhi(const double& phi) {
  m_phi = phi;
}

void Track::setZ0(const double& z0) {
  m_z0 = z0;
}

bool Track::setTruth(const int& truth) {
  if (truth == 1) {
    m_truth_matched = true;
  } else {
    m_truth_matched = false;
  }
}

void Track::setExtrapolationPoint(const std::vector<double>& pos, const int& layer) {
  switch (layer) {
        case 0:
          l0.SetXYZ(pos.at(0), pos.at(1), pos.at(2));
          break;
        case 1:
          l1.SetXYZ(pos.at(0), pos.at(1), pos.at(2));
          break;
        case 2:
          l2.SetXYZ(pos.at(0), pos.at(1), pos.at(2));
          break;
        case 3:
          l3.SetXYZ(pos.at(0), pos.at(1), pos.at(2));
          break;
        default:
          std::cout<<"Track::setExtrapolationPoint: invalid layer value, extrapolation point unset." << std::endl;
    }
}
