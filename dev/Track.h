/**
* @Author Alexander Leopold (alexander.leopold@cern.ch)
* @brief  Describe the class here.
*/

#ifndef TRACK_H
#define TRACK_H

// STL includes
#include <vector>
#include "TObject.h"
#include "TVector3.h"

class Track : public TObject{

public:
  Track();
  ~Track();

  double pt();
  double e();
  double eta();
  double phi();
  double z0();
  bool isTruth();
  TVector3 extrapolate(const int& layer);

  void setPt(const double& pt);
  void setE(const double& e);
  void setEta(const double& eta);
  void setPhi(const double& phi);
  void setZ0(const double& z0);
  void setTruth(const int& truth);
  void setExtrapolationPoint(const std::vector<double>& pos, const int& layer);


private:
  double m_pt;
  double m_energy;
  double m_eta;
  double m_phi;
  double m_z0;
  bool m_truth_matched;
  TVector3 l0;
  TVector3 l1;
  TVector3 l2;
  TVector3 l3;
  ClassDef(Track, 1);
};

#endif //TRACK_H
