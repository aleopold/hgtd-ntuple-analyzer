find "."  -type f -name "._*" -delete #delete files produced by mac os

#path="/data/atlas/aleopold/hgtd/user.aleopold.hgtd_bbar_mu200_DeltaZandLeadingTime.2018_02_27_1750_HGTDHitAnalysis"
#ls -d  $path/* > INPUT_LIST_lpnatlas.txt
# source getTTlist.sh

rm *.so
rm *.pcm
rm *.d

### USAGE ###
#expected input for this script:
#./execute <true/false> <config file name>
#true means the selector is executed to produce a root file

#false means only the plotting scripts are executed, which are called in the second half of Main.C

process_data=$1
config_file=$2
#if no input for the config file name is provided, the stanard one is used
if [ -z "$config_file" ]; then
    config_file="globalInput.config"
fi


#create dir where plots will be stored, the name of the directory that should be created is taken from the config file (name provided as input)
dir=`cat globalInput.config | grep -v '^#' | grep "output_plot_path" | cut -d ":" -f2`
if [ $process_data == "false" ]
then
  if [ -d "$dir" ]; then
    # Control will enter here if $DIRECTORY exists.
    PS3='Directory ${dir} does exist, do you want to possibly overwrite it?'
    options=("yes" "no")
    select opt in "${options[@]}"
    do
      case $opt in
        "yes")

        break
        ;;
        "no")
        kill -INT $$
        break
        ;;
        *) echo invalid option;;
      esac
    done
  fi
  if [ ! -d "$dir" ]; then
    echo "creating directory ${dir}"
    mkdir -p $dir
  fi
fi

##executable is called here in interpreted ROOT, logfiles are only kept when submission to batch
root -l -q -b Main.C'("helper/DataSelector.C+", "Data", '"${process_data}"', "'${config_file}'")'
# root -l -q -b Main.C'("helper/DataSelector.C+", "Data", '"${process_data}"', "'${config_file}'")' | tee logfile.log
