#path="/data/atlas/aleopold/hgtd/user.aleopold.hgtd_bbar_mu200_DeltaZandLeadingTime.2018_02_27_1750_HGTDHitAnalysis"
#ls -d  $path/* > INPUT_LIST_lpnatlas.txt
# source getTTlist.sh

rm *.so
rm *.pcm
rm *.d

process_data=$1
config_file=$2

root -l -q -b Main.C'("helper/DataSelector.C+", "Data", '"${process_data}"', "'${config_file}'")'
# root -l -q -b Main.C'("helper/DataSelector.C+", "Data", '"${process_data}"', "'${config_file}'")' | tee logfile.log
