# path="/Users/aleopold/ATLAS/hgtd/"
# path="/afs/cern.ch/user/a/aleopold/public/hgtd_data/"
# path="/afs/cern.ch/work/a/aleopold/private/hgtd/jul_18_dataCollection/run/testsample/"
# path="/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_dijet_mu200_DATA.2018_07_19_1858_HGTDHitAnalysis/"
# path="/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_dijet_mu200_DATA.2018_07_25_2331_HGTDHitAnalysis/"
# path="/data/atlas/aleopold/hgtd/user.aleopold.hgtd_dijet_mu200_DATA.2018_07_19_1858_HGTDHitAnalysis"
# path="/data/atlas/aleopold/hgtd/user.aleopold.hgtd_dijet_mu200_DATA.2018_07_25_2331_HGTDHitAnalysis"

#data on lxplus/eos
##dijet
#NOTE old ----
# path="/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_dijet_mu200_DATA_withPulse.2018_07_27_1712_HGTDHitAnalysis"
#path="/eos/atlas/user/l/laforge/hgtd_processed/user.aleopold.hgtd_VBFinv_mu200_DATA.2018_08_30_1651_HGTDHitAnalysis/"
#path="/afs/cern.ch/work/l/laforge/user.aleopold.hgtd_VBFinv_mu200_DATA.2018_08_30_1651_HGTDHitAnalysis/"
# path="/eos/atlas/user/l/laforge/hgtd_processed/user.aleopold.hgtd_ttbar_mu200_DATA.2018_08_31_1806_HGTDHitAnalysis/"
##----------------

##----------------
##dijet with PU times
# path="/eos/atlas/atlascerngroupdisk/det-hgtd/samples/ntuples_aleopold/user.aleopold.hgtd_dijet_mu200_DATA.2018_09_02_2303_HGTDHitAnalysis"

##one file without pulse simulation (for event display)
# path="/afs/cern.ch/work/a/aleopold/private/hgtd/jul_18_dataCollection/run/nopulse"

##VBFinv mu200
# path="/eos/atlas/atlascerngroupdisk/det-hgtd/samples/ntuples_aleopold/user.aleopold.hgtd_VBFinv_mu200_DATA.2018_09_02_2257_HGTDHitAnalysis"

## Ztautau
# path="/eos/atlas/atlascerngroupdisk/det-hgtd/samples/ntuples_aleopold/user.aleopold.hgtd_Ztautau_mu200_DATA.2018_09_03_1858_HGTDHitAnalysis"

## ttbar
# path="/eos/atlas/atlascerngroupdisk/det-hgtd/samples/ntuples_aleopold/user.aleopold.hgtd_ttbar_mu200_DATA.2018_09_03_1902_HGTDHitAnalysis"

## VBF4l
# path="/eos/atlas/atlascerngroupdisk/det-hgtd/samples/ntuples_aleopold/user.aleopold.hgtd_VBF4lep_mu200_DATA.2018_09_04_1500_HGTDHitAnalysis"

## JZ2W
# path="/eos/atlas/atlascerngroupdisk/det-hgtd/samples/ntuples_aleopold/user.aleopold.hgtd_dijetJZ2W_mu200_DATA.2018_09_04_2130_HGTDHitAnalysis"
## JZ3W
# path="/eos/atlas/atlascerngroupdisk/det-hgtd/samples/ntuples_aleopold/user.aleopold.hgtd_dijetJZ3W_mu200_DATA.2018_09_04_2130_HGTDHitAnalysis"
## JZ4W
# path="/eos/atlas/atlascerngroupdisk/det-hgtd/samples/ntuples_aleopold/user.aleopold.hgtd_dijetJZ4W_mu200_DATA.2018_09_04_1258_HGTDHitAnalysis"

##ttbar mu=0
# path="/eos/atlas/atlascerngroupdisk/det-hgtd/samples/ntuples_aleopold/user.aleopold.hgtd_ttbar_mu0_DATA.2018_11_25_1507_HGTDHitAnalysis"

###TDRtests step2p2
# path="/afs/cern.ch/work/a/aleopold/private/hgtd/jul_18_dataCollection/run"
###TDRtests step3p0
path="/afs/cern.ch/work/a/aleopold/private/hgtd/ana_step3/run/dijet_mu0"

ls -d $path/*.root   > INPUT_LIST.txt
