//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Jul  8 22:26:24 2018 by ROOT version 6.13/01
// from TChain Data/
//////////////////////////////////////////////////////////

#ifndef DataSelector_h
#define DataSelector_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>




// Headers needed by this particular selector
#include <vector>
#include <TVector3.h>
#include <TEnv.h>
#include <TString.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGraph.h>

#include <fstream>
#include <memory>
// #include <mutex>

class Event;
class Hit;
class Jet;
class Track;

class DataSelector : public TSelector {
private:
  // std::mutex m_mutex;
  //parameters for subselection
  TString m_output_filename;
  TString m_output_plot_path;
  double m_min_eta;
  double m_max_eta;
  double m_min_track_pt;
  double m_min_jet_pt;
  double m_max_jet_pt;
  double m_perimeter_radius;
  double m_perimeter_delta_r;
  double m_event_weight;
  double m_global_t0 = -999.;
  ofstream m_cluser_parameter_file;
  double m_square_side;
  double m_time_res_tolerance;
  bool m_debug = false;

  std::vector <double> m_eff_rpt_cut;

  TH1F *hist_isolated_tracks = nullptr;
  //event display
  TH2F *hist_hits_first_layer = nullptr;
  TGraph *graph_tracks_fist_layer = nullptr;
  //distance in time between different track timing methods
  TH1F *hist_time_distance_clustassoc = nullptr;
  //check how many tracks have a cluster time
  TH1F *hist_track_has_cluster_time = nullptr;
  TH1F *hist_track_has_cluster_time_cut = nullptr;
  //time resolution (wrt truth) of HS tracks
  TH1F *hist_timeres_assoc_5mm = nullptr;
  TH1F *hist_timeres_assoc = nullptr;
  TH1F *hist_timeres_clust = nullptr;
  TH1F *hist_timeres_clust_rms = nullptr;
  TH1F *hist_timeres_clust_radius = nullptr;
  //time res for hs and pu tracks, no fakes
  TH1F *hist_timeres_clust_all = nullptr;

  TH1F *hist_timeres_clust_cuts = nullptr;
  //average number of HS tracks in top m_max_leading_tracks leading tracks
  unsigned int m_max_leading_tracks = 10;
  TH1F *hist_leading_tracks_are_hs = nullptr;
  //entry of first HS track in top 10 leading
  TH1F *hist_first_hs = nullptr;
  //average fraction of HS tracks in event generally
  TH1F *hist_frac_hs_tracks = nullptr;
  //delta pT of first HS to leading
  TH1F *hist_first_hs_deltapt = nullptr;
  //time distribution of hits in cluster close to truth and far away
  TH1F *hist_timedist_good_cluster = nullptr;
  TH1F *hist_timedist_bad_cluster = nullptr;

  //number of hits around hs and pu tracks
  TH1F *hist_n_hits_hs_tracks = nullptr;
  TH1F *hist_n_hits_pu_tracks = nullptr;
  //number of jets in hgtd acceptance
  TH1F *hist_jets_in_hgtd = nullptr;
  //time resolution of leading and subleading jets in hgtd
  TH1F *hist_jet_time_resolution = nullptr;
  TH1F *hist_jet_time_resolution_PUjets = nullptr; //same when selecting only PU
  TH1F *hist_jet_time_resolution_HSjets = nullptr;
  //correlations between vertex time res and others
  TGraph *graph_dt_rmean = nullptr;
  TGraph *graph_dt_size = nullptr;
  TGraph *graph_dt_relative_size = nullptr;
  TGraph *graph_dt_time_rms = nullptr;
  TGraph *graph_rmean_relative_size = nullptr;
  int m_graph_point_i;
  //check if leading track within leading jet is HS
  TH1F* hist_jet_leading_track_hs = nullptr;
  //leading jet is in HGTD
  TH1F* hist_leadingjet_in_HGTD = nullptr;
  //leading jet that is in HGTD is truth matched
  TH1F* hist_leadingjet_is_hs = nullptr;
  //how many jets have a time
  TH1F* hist_event_has_t0 = nullptr;
  //fraction of ghost tracks with a time (when using cuts)
  TH1F* hist_ghosttracks_with_time = nullptr;
  //fraction of PU tracks in HS jets
  TH2F* hist_frac_pu_tracks_per_jet_over_eta = nullptr;
  //check if pu leading jets are by accident cut
  TH1F* hist_t0hsornot = nullptr;
  //RPT
  TH1F *hist_rpt_hs_ghost_assoc_t0 = nullptr;
  TH1F *hist_rpt_pu_ghost_assoc_t0 = nullptr;
  TH1F *hist_rpt_hs_ghost_assoc_highpt_t0 = nullptr;
  TH1F *hist_rpt_pu_ghost_assoc_highpt_t0 = nullptr;
  TEfficiency* hist_eff_hs_ghost_assoc_t0 = nullptr;
  TEfficiency* hist_eff_pu_ghost_assoc_t0 = nullptr;
  TEfficiency* hist_eff_hs_ghost_assoc_highpt_t0 = nullptr;
  TEfficiency* hist_eff_pu_ghost_assoc_highpt_t0 = nullptr;

  TEfficiency* hist_eff_hs_ghost_assoc = nullptr;
  TEfficiency* hist_eff_pu_ghost_assoc = nullptr;
  TEfficiency* hist_eff_hs_ghost_assoc_highpt = nullptr;
  TEfficiency* hist_eff_pu_ghost_assoc_highpt = nullptr;
  //cluster sizes in leading jets, split into HS and PU
  TH1F *hist_leadingjet_clustersize_hs = nullptr;
  TH1F *hist_leadingjet_clustersize_pu = nullptr;
  //cluster sizes in ALL jets, split into HS and PU
  TH1F *hist_jet_clustersize_hs = nullptr;
  TH1F *hist_jet_clustersize_pu = nullptr;
  //plots for ariel's selection
  TH1F *hist_mjj = nullptr;
  TH1F *hist_mjj_selection_is_hs = nullptr;

  TH1F *hist_track_type = nullptr;

  TH1F *hist_total_number_of_hits = nullptr;
  TH1F *hist_number_of_hits_in_deltaR = nullptr;

  TH1F *hist_track_throuth_4_hits = nullptr;
  TH1F *hist_track_throuth_4_hits_timeres = nullptr;
  TH1F *hist_track_throuth_4_hits_timeres_closest = nullptr;
  TH1F *hist_track_throuth_4_hits_timeres_leastsquare = nullptr;
  TH1F *hist_track_throuth_4_hits_timeres_nofakes = nullptr;
  TH1F *hist_track_throuth_4or3_hits = nullptr;
  TH1F *hist_track_throuth_4or3_hits_timeres_closest = nullptr;
  TH1F *hist_track_throuth_4or3_hits_timeres_leastsquare = nullptr;
  TH1F *hist_track_throuth_4or3_hits_timeres_leastsquare_nocombos = nullptr;
  TH1F *hist_track_throuth_4or3_hits_num_candidates = nullptr;
  TH2F *hist_layers_filled_over_eta = nullptr;
  TEfficiency* eff_tracking_over_eta = nullptr;
  TEfficiency* eff_tracking_over_pt = nullptr;

  //kinematics of objects
  TH1F *hist_track_pt = nullptr;

  TH1F *hist_jet_pt = nullptr;
  TH1F *hist_jet_eta = nullptr;
  TH1F *hist_jet_fabseta = nullptr;

  //dirk's question about hit times in and outside of jets
  TH1F* hist_hits_in_hs_jets = nullptr;
  TH1F* hist_hits_in_pu_jets = nullptr;
  TH1F* hist_hits_in_no_jets = nullptr;

  TH1F* hist_deltar_extrap0_extrap3 = nullptr;


  TTree *m_cluster_prop_tree = nullptr;
  double m_cluster_trackpt;
  double m_cluster_tracketa;
  int m_cluster_nhits;
  double m_cluster_time_res;
  double m_cluster_time;
  double m_cluster_timeRMS;
  double m_cluster_radiusMean;
  double m_cluster_radiusRMS;
  double m_cluster_angleRMS;

  //objects of each event
  std::vector <std::shared_ptr<Track>> m_tracks;
  std::vector <std::shared_ptr<Hit>> m_hits;
  std::vector <std::shared_ptr<Jet>> m_jets;
  std::shared_ptr<Event> m_event;

  int m_event_count = 0;

public:



  TTreeReader     fReader;  //!the tree reader
  TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain



  // Readers to access the data (delete the ones you do not need).
  TTreeReaderValue<Int_t> m_eventnumber = {fReader, "m_eventnumber"};
  TTreeReaderValue<Int_t> m_runnumber = {fReader, "m_runnumber"};
  TTreeReaderValue<Int_t> m_n_vertices = {fReader, "m_n_vertices"};
  TTreeReaderValue<Double_t> m_vertex_x = {fReader, "m_vertex_x"};
  TTreeReaderValue<Double_t> m_vertex_y = {fReader, "m_vertex_y"};
  TTreeReaderValue<Double_t> m_vertex_z = {fReader, "m_vertex_z"};
  TTreeReaderValue<Double_t> m_truth_vertex_x = {fReader, "m_truth_vertex_x"};
  TTreeReaderValue<Double_t> m_truth_vertex_y = {fReader, "m_truth_vertex_y"};
  TTreeReaderValue<Double_t> m_truth_vertex_z = {fReader, "m_truth_vertex_z"};
  TTreeReaderValue<Double_t> m_truth_vertex_time = {fReader, "m_truth_vertex_time"};
  TTreeReaderArray<float> m_hit_x = {fReader, "m_hit_x"};
  TTreeReaderArray<float> m_hit_y = {fReader, "m_hit_y"};
  TTreeReaderArray<float> m_hit_z = {fReader, "m_hit_z"};
  TTreeReaderArray<double> m_hit_raw_time = {fReader, "m_hit_raw_time"};
  // TTreeReaderArray<int> m_hit_has_pulse = {fReader, "m_hit_has_pulse"};
  TTreeReaderArray<double> m_hit_pulse_time = {fReader, "m_hit_pulse_time"};
  TTreeReaderArray<double> m_hit_pulse_resolution = {fReader, "m_hit_pulse_resolution"};
  TTreeReaderArray<int> m_hit_layer = {fReader, "m_hit_layer"};
  TTreeReaderArray<double> m_track_pt = {fReader, "m_track_pt"};
  TTreeReaderArray<double> m_track_e = {fReader, "m_track_e"};
  TTreeReaderArray<double> m_track_eta = {fReader, "m_track_eta"};
  TTreeReaderArray<double> m_track_phi = {fReader, "m_track_phi"};
  TTreeReaderArray<double> m_track_z0 = {fReader, "m_track_z0"};
  TTreeReaderArray<double> m_track_vz = {fReader, "m_track_vz"};
  TTreeReaderArray<double> m_track_d0 = {fReader, "m_track_d0"};
  TTreeReaderArray<int> m_track_is_truthmatched = {fReader, "m_track_is_truthmatched"};
  TTreeReaderArray<int> m_track_is_truthstatus = {fReader, "m_track_is_truthstatus"};
  TTreeReaderArray<double> m_track_vx_time = {fReader, "m_track_vx_time"};
  TTreeReaderArray<unsigned long> m_track_hashid = {fReader, "m_track_hashid"};
  TTreeReaderArray<int> m_track_pdgid = {fReader, "m_track_pdgid"};
  TTreeReaderArray<vector<double>> m_track_extrapolated_pos_l0 = {fReader, "m_track_extrapolated_pos_l0"};
  TTreeReaderArray<vector<double>> m_track_extrapolated_pos_l1 = {fReader, "m_track_extrapolated_pos_l1"};
  TTreeReaderArray<vector<double>> m_track_extrapolated_pos_l2 = {fReader, "m_track_extrapolated_pos_l2"};
  TTreeReaderArray<vector<double>> m_track_extrapolated_pos_l3 = {fReader, "m_track_extrapolated_pos_l3"};
  TTreeReaderArray<vector<double>> m_track_extrapolated_pt_l0 = {fReader, "m_track_extrapolated_pt_l0"};
  TTreeReaderArray<vector<double>> m_track_extrapolated_pt_l1 = {fReader, "m_track_extrapolated_pt_l1"};
  TTreeReaderArray<vector<double>> m_track_extrapolated_pt_l2 = {fReader, "m_track_extrapolated_pt_l2"};
  TTreeReaderArray<vector<double>> m_track_extrapolated_pt_l3 = {fReader, "m_track_extrapolated_pt_l3"};
  TTreeReaderArray<int> m_track_extrapolated_fillfactor_l0 = {fReader, "m_track_extrapolated_fillfactor_l0"};
  TTreeReaderArray<int> m_track_extrapolated_fillfactor_l1 = {fReader, "m_track_extrapolated_fillfactor_l1"};
  TTreeReaderArray<int> m_track_extrapolated_fillfactor_l2 = {fReader, "m_track_extrapolated_fillfactor_l2"};
  TTreeReaderArray<int> m_track_extrapolated_fillfactor_l3 = {fReader, "m_track_extrapolated_fillfactor_l3"};
  TTreeReaderArray<double> m_jet_pt = {fReader, "m_jet_pt"};
  TTreeReaderArray<double> m_jet_e = {fReader, "m_jet_e"};
  TTreeReaderArray<double> m_jet_eta = {fReader, "m_jet_eta"};
  TTreeReaderArray<double> m_jet_phi = {fReader, "m_jet_phi"};
  TTreeReaderArray<int> m_jet_is_hs = {fReader, "m_jet_is_hs"};
  TTreeReaderArray<int> m_jet_is_pu = {fReader, "m_jet_is_pu"};
  TTreeReaderArray<int> m_jet_is_qcd = {fReader, "m_jet_is_qcd"};
  TTreeReaderArray<int> m_jet_is_stoch = {fReader, "m_jet_is_stoch"};
  TTreeReaderArray<vector<unsigned long>> m_jet_ghost_track_hashid = {fReader, "m_jet_ghost_track_hashid"};


  DataSelector(TTree * /*tree*/ =0) {

  }

  // void SetConfigName(TString config_file_name="globalInput.config") {
  //
  // }
  virtual ~DataSelector() { }
  virtual Int_t   Version() const { return 2; }
  virtual void    Begin(TTree *tree);
  virtual void    SlaveBegin(TTree *tree);
  virtual void    Init(TTree *tree);
  virtual Bool_t  Notify();
  virtual Bool_t  Process(Long64_t entry);
  virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
  virtual void    SetOption(const char *option) { fOption = option; }
  virtual void    SetObject(TObject *obj) { fObject = obj; }
  virtual void    SetInputList(TList *input) { fInput = input; }
  virtual TList  *GetOutputList() const { return fOutput; }
  virtual void    SlaveTerminate();
  virtual void    Terminate();

  //////////////////////////////////////////////////////
  //////////// additionally defined methods ////////////
  //////////////////////////////////////////////////////
  void SetupEvent();
  void SetupObjects();
  void SetupJets();

  void hitsInsideJets(); //time distro of hits that can be found inside jets

  void studyHitTrackAssociation();
  void studyClusteringProperties();
  void studyHStracksProperties();
  void studyJetTracks();
  void getJsonData();
  void getKinematics();
  void changeOfExtrapolationVector();


  void findOverlapCells();
  void findOverlapCells2();
  void findOverlapCells3();
  // void findOverlapCells4();
  int findOverlapCells4();
  int findOverlapCells5();
  // --- methods for "hgtd tracking" ----
  map<double, double> combineFourHits(std::vector<std::vector<std::shared_ptr<Hit>>>& candidates, Track* track);
  map<double, double> combineThreeHits(std::vector<std::vector<std::shared_ptr<Hit>>>& candidates, Track* track);
  map<double, double> combineTwoHits(std::vector<std::vector<std::shared_ptr<Hit>>>& candidates, Track* track);

  // map<double, double> combineFourHits(std::vector<std::vector<std::shared_ptr<Hit>>>& candidates, Track* track);
  map<double, double> combineThreeHits(std::vector<std::vector<std::shared_ptr<Hit>>>& candidates, Track* track, const int& initial_layer, const std::vector<int>& used_layers);
  map<double, double> combineTwoHits(std::vector<std::vector<std::shared_ptr<Hit>>>& candidates, Track* track, const int& initial_layer, const std::vector<int>& used_layers);



  //---- NOTE don't use the following two at the same time!!! ----
  void leadingJetTimes();
  void clusteringInJets();
  void leadingJetTimesTruthTrackTime();
  //--------------------------------------------
  bool arielSelection();
  bool leadingTrackIsHS(int& n_goodtracks);


  bool isGoodTrack(Track* trk);

  /**
     * @brief Correct the time of a hit for TOF relative to a certain z, which
     * can be a vertex or a specific track.
     *
     * @param [in] hit Hit for which the time should be corrected;
     * @param [in] z Position along the beamline w.r.t. which TOF is calculated.
     *
     */
  double correctTime(Hit* hit, const double& z);
  // void ClassicHitAssociation(std::unique_ptr<Track> & track, const double& radius);
  void ClassicHitAssociation(Track* track, const double& radius);

  double calculateRpT(Jet* jet);
  double calculateRpT(Jet* jet, double t0);
  void rptPlotting();

  void eventDisplay(const std::vector<double>& hit_eta, const std::vector<double>& hit_phi, const int & slice);
  void eventDisplay(const std::vector<double>& hit_eta, const std::vector<double>& hit_phi, const std::vector<double>& track_eta, const std::vector<double>& track_phi,const int & slice);
  void eventDisplayVicinity(const std::vector<double>& hit_eta, const std::vector<double>& hit_phi, const int& track_id);

  //////////////////////////////////////////////////////
  //////////////////////////////////////////////////////
  // ClassDef(DataSelector,0);
  ClassDef(DataSelector,1);

};

#endif

#ifdef DataSelector_cxx
void DataSelector::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the reader is initialized.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  fReader.SetTree(tree);
}

Bool_t DataSelector::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}


#endif // #ifdef DataSelector_cxx
