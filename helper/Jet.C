/**
* @Author Alexander Leopold (alexander.leopold@cern.ch)
* @brief  Describe the class here.
*/

#ifndef JET_H
#define JET_H

// STL includes
#include <vector>
#include <iostream>
#include "TVector3.h"

class Jet {

public:

  double pt() const;
  double e() const;
  double eta() const;
  double phi() const;
  int isHS() const;
  int isPU() const;
  int isQCD() const;
  int isStoch() const;
  double jetTime() const;
  std::vector<size_t> getGhostHashes() const;

  void setPt(const double& pt);
  void setE(const double& e);
  void setEta(const double& eta);
  void setPhi(const double& phi);
  void setJetTime(const double& t) {m_jet_time = t;}
  void setTruthInfo(const int& ishs, const int& ispu, const int& isqcd, const int& isstoch);
  void setGhostHashes(const std::vector<size_t>& hashes);

  bool operator< (const Track& rhs) const { return m_pt < rhs.pt(); }

private:
  double m_pt;
  double m_energy;
  double m_eta;
  double m_phi;
  double m_jet_time;
  int m_is_hs;
  int m_is_pu;
  int m_is_qcd;
  int m_is_stoch;
  std::vector<size_t> m_ghost_hash_id;
};

double Jet::pt() const {
  return m_pt;
}

double Jet::e() const {
  return m_energy;
}

double Jet::eta() const {
  return m_eta;
}

double Jet::phi() const {
  return m_phi;
}


int Jet::isHS() const {
  return m_is_hs;
}

int Jet::isPU() const {
  return m_is_pu;
}

int Jet::isQCD() const {
  return m_is_qcd;
}

int Jet::isStoch() const {
  return m_is_stoch;
}

double Jet::jetTime() const {
  return m_jet_time;
}

std::vector<size_t> Jet::getGhostHashes() const {
  return m_ghost_hash_id;
}


void Jet::setPt(const double& pt) {
  m_pt = pt;
}

void Jet::setE(const double& e) {
  m_energy = e;
}

void Jet::setEta(const double& eta) {
  m_eta = eta;
}

void Jet::setPhi(const double& phi) {
  m_phi = phi;
}

void Jet::setTruthInfo(const int& ishs, const int& ispu, const int& isqcd, const int& isstoch) {
  m_is_hs = ishs;
  m_is_pu = ispu;
  m_is_qcd = isqcd;
  m_is_stoch = isstoch;
}

void Jet::setGhostHashes(const std::vector<size_t>& hashes) {
  m_ghost_hash_id = hashes;
}




#endif //JET_H
