#!/usr/bin/env python
import os,sys
import subprocess

def killjobs():
  proc = subprocess.Popen(["bjobs"], stdout=subprocess.PIPE, shell=True)
  (out, err) = proc.communicate()

#  print "output: ",  out
  lines = out.split('\n')
  for line in lines[1:]:
    if not "ntuple" in line:
      continue
    splitline = line.split(" ")
    print splitline[0]
    subprocess.call(["bkill", splitline[0]])

def main():
  print "Are you sure you want to kill all batch jobs? (y/n)"
  msg = raw_input()
  
  if (msg=='y' or msg=='Y'):
    killjobs()
  else:
    return


if __name__ == "__main__":
  main()
