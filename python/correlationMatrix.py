import matplotlib.pyplot as plt
import pandas as pd
from pandas.tools.plotting import radviz, parallel_coordinates, andrews_curves
import numpy as np
from root_pandas import read_root

tree = read_root('../output.root', 'cluster')


# plt.figure(figsize=(20,6))
# plt.scatter(x=tree['m_cluster_trackpt'],y=tree['m_cluster_tracketa'], alpha=0.2)
# plt.xlabel('m_cluster_trackpt')
# plt.ylabel('m_cluster_tracketa')

vars = ["m_cluster_time_res", "m_cluster_nhits", "m_cluster_timeRMS", "m_cluster_radiusMean", "m_cluster_radiusRMS", "m_cluster_angleRMS"]

fig, axes = plt.subplots(ncols=len(vars), nrows=len(vars), figsize=(10,10))

# axes = axes.ravel()



for i, var1 in enumerate(['m_cluster_time_res']):
    for j, var2 in enumerate(vars):
        # axes[i, j].scatter(x=tree[var1],y=tree[var2], alpha=0.2)
        # axes[i, j].set_xlabel(var1)
        # axes[i, j].set_ylabel(var2)
        plt.figure(figsize=(15, 15))
        plt.scatter(x=tree[var1],y=tree[var2], alpha=0.2)
        plt.xlabel(var1)
        plt.ylabel(var2)
        plt.savefig("plots/{0}_{1}.pdf".format(var1, var2))
        plt.close()

# axes[0].scatter(x=tree['m_cluster_time_res'],y=tree['m_cluster_nhits'], alpha=0.2)
# axes[0].set_ylabel('m_cluster_time_res')
# axes[0].set_xlabel('m_cluster_nhits')


# plt.tight_layout()
# plt.show()
