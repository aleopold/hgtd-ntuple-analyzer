import ast

def getInput():


    f = open('input.txt', 'r')
    string = f.readlines()

    vxtime = -999
    trk_e0x = -999
    trk_e0y = -999
    trk_e0x2 = -999
    trk_e0y2 = -999
    trk_e0x3 = -999
    trk_e0y3 = -999
    trk_e0x4 = -999
    trk_e0y4 = -999
    layer0 = []
    layer1 = []
    layer2 = []
    layer3 = []
    for s in string:
        if s.startswith("truth"):
            s.rstrip()
            v = s.split(' ')
            vxtime = float(v[-1])
        if s.startswith("main track position"):
            s.rstrip()
            v = s.split(' ')
            layernumber = int(v[4][0])
            if layernumber == 0:
                trk_e0x = float(v[5])
                trk_e0y = float(v[6])
            if layernumber == 1:
                trk_e0x2 = float(v[5])
                trk_e0y2 = float(v[6])
            if layernumber == 2:
                trk_e0x3 = float(v[5])
                trk_e0y3 = float(v[6])
            if layernumber == 3:
                trk_e0x4 = float(v[5])
                trk_e0y4 = float(v[6])
        if s.startswith('layer') and not 'hits' in s:
            s.rstrip()
            layernumber = int(s.split('.')[0][-1])
            if layernumber == 0:
                x = s.split('(')[1]
                x = x.split(')')[0]
                x = ast.literal_eval(x)
                layer0.append(x)
            if layernumber == 1:
                x = s.split('(')[1]
                x = x.split(')')[0]
                x = ast.literal_eval(x)
                layer1.append(x)
            if layernumber == 2:
                x = s.split('(')[1]
                x = x.split(')')[0]
                x = ast.literal_eval(x)
                layer2.append(x)
            if layernumber == 3:
                x = s.split('(')[1]
                x = x.split(')')[0]
                x = ast.literal_eval(x)
                layer3.append(x)

    return vxtime, trk_e0x, trk_e0y, trk_e0x2, trk_e0y2, trk_e0x3, trk_e0y3, trk_e0x4, trk_e0y4, layer0, layer1, layer2, layer3


getInput()
