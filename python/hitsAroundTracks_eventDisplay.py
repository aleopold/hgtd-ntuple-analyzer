#!/usr/bin/env python
import ROOT
# import math
import ast

def getInput():
    f = open('input.txt', 'r')
    string = f.readlines()

    vxtime = -999
    trk_e0x = -999
    trk_e0y = -999
    trk_e0x2 = -999
    trk_e0y2 = -999
    trk_e0x3 = -999
    trk_e0y3 = -999
    trk_e0x4 = -999
    trk_e0y4 = -999
    layer0 = []
    layer1 = []
    layer2 = []
    layer3 = []
    for s in string:
        if s.startswith("truth"):
            s.rstrip()
            v = s.split(' ')
            vxtime = float(v[-1])
        if s.startswith("main track position"):
            s.rstrip()
            v = s.split(' ')
            layernumber = int(v[4][0])
            if layernumber == 0:
                trk_e0x = float(v[5])
                trk_e0y = float(v[6])
            if layernumber == 1:
                trk_e0x2 = float(v[5])
                trk_e0y2 = float(v[6])
            if layernumber == 2:
                trk_e0x3 = float(v[5])
                trk_e0y3 = float(v[6])
            if layernumber == 3:
                trk_e0x4 = float(v[5])
                trk_e0y4 = float(v[6])
        if s.startswith('layer') and not 'hits' in s:
            s.rstrip()
            layernumber = int(s.split('.')[0][-1])
            if layernumber == 0:
                x = s.split('(')[1]
                x = x.split(')')[0]
                x = ast.literal_eval(x)
                layer0.append(x)
            if layernumber == 1:
                x = s.split('(')[1]
                x = x.split(')')[0]
                x = ast.literal_eval(x)
                layer1.append(x)
            if layernumber == 2:
                x = s.split('(')[1]
                x = x.split(')')[0]
                x = ast.literal_eval(x)
                layer2.append(x)
            if layernumber == 3:
                x = s.split('(')[1]
                x = x.split(')')[0]
                x = ast.literal_eval(x)
                layer3.append(x)

    return vxtime, trk_e0x, trk_e0y, trk_e0x2, trk_e0y2, trk_e0x3, trk_e0y3, trk_e0x4, trk_e0y4, layer0, layer1, layer2, layer3
ROOT.gStyle.SetOptStat(0)


vxtime, trk_e0x, trk_e0y, trk_e0x2, trk_e0y2, trk_e0x3, trk_e0y3, trk_e0x4, trk_e0y4, layer0, layer1, layer2, layer3 = getInput()

print layer0, layer1, layer2, layer3
# vxtime = -0.0391521
#
# trk_e0x = 138.399
# trk_e0y = 36.9049
# layer0 = []
# layer0.append([137.5, 37, 0.438766])
# layer0.append([138.5, 37, 0.444112])
# layer0.append([137.5, 35, 0.179327])
# layer0.append([139.5, 39, 0.46458])
# trk_e0x2 = 138.72
# trk_e0y2 = 36.9887
# layer1 = []
# layer1.append([140.5, 37, 0.414505])
# layer1.append([138.5, 35, 0.143216])
# layer1.append([138.5, 37, 0.478317])
# trk_e0x3 = 139.044
# trk_e0y3 = 37.0728
# layer2 = []
# layer2.append([139.5, 37, 0.477094])
# layer2.append([138.5, 35, 0.129589])
# layer2.append([139.5, 40, 0.402619])
# trk_e0x4 = 139.36
# trk_e0y4 = 37.1567
# layer3 = []
# layer3.append([141.5, 39, 0.333622])
# layer3.append([139.5, 40, 0.382414])
# layer3.append([138.5, 35, 0.128696])
# layer3.append([138.5, 36, 0.615579])
# layer3.append([139.5, 35, 0.620498])
# layer3.append([139.5, 37, 0.482625])

def criterium(hittime, vxtime):
    if abs(hittime - vxtime) < 0.06:
        return True
    else:
        return False

c = ROOT.TCanvas('', '', 800, 800)
c.Divide(2,2)
c.cd(1)
ROOT.gPad.SetTicks()

sizeHisto = ROOT.TH1F("histo", ";x[mm];y[mm]", 7, -4, 3)
sizeHisto.SetLineColor(ROOT.kWhite   )
sizeHisto.SetTitle('Layer 0')


trk_position = ROOT.TGraph()
trk_position.SetPoint(1, 0, 0)
trk_position.SetMarkerStyle(34)
trk_position.SetMarkerColor(ROOT.kBlack)
trk_position.SetMarkerSize(2)



graph_good = ROOT.TGraph()
graph_good.SetMarkerStyle(24)
graph_good.SetMarkerColor(ROOT.kBlue)
graph_good.SetMarkerSize(3)
graph_bad = ROOT.TGraph()
graph_bad.SetMarkerStyle(26)
graph_bad.SetMarkerColor(ROOT.kRed)
graph_bad.SetMarkerSize(3)
goodi = 0
badi = 0
for entry in layer0:
    # if abs(vxtime - entry[2] - 0.409) < 0.06:
    if criterium(entry[2], vxtime):
        graph_good.SetPoint(goodi, entry[0] - trk_e0x, entry[1] - trk_e0y)
        goodi += 1
    else:
        graph_bad.SetPoint(badi, entry[0] - trk_e0x, entry[1] - trk_e0y)
        badi += 1
        print badi, entry[0] - trk_e0x, entry[1] - trk_e0y

# c = ROOT.TCanvas('', '', 800, 800)
# c.SetTicks()
sizeHisto.Draw()
sizeHisto.SetAxisRange(-4, 4, "X")
sizeHisto.SetAxisRange(-4, 4, "Y")
el1 = ROOT.TEllipse(0.,0.,1.,1.);
el2 = ROOT.TEllipse(0.,0.,2.,2.);
el3 = ROOT.TEllipse(0.,0.,3.,3.);
el1.Draw("L SAME")
el2.Draw("SAME")
el3.Draw("SAME")
trk_position.Draw("P SAME")
if graph_good.GetN() > 0:
    graph_good.Draw('P SAME')
if graph_bad.GetN() > 0:
    graph_bad.Draw('P SAME')
###########################

c.cd(2)
ROOT.gPad.SetTicks()

sizeHisto2 = ROOT.TH1F("histo", ";x[mm];y[mm]", 7, -4, 3)
sizeHisto2.SetLineColor(ROOT.kWhite   )
sizeHisto2.SetTitle('Layer 1')


trk_position2 = ROOT.TGraph()
trk_position2.SetPoint(1, 0, 0)
trk_position2.SetMarkerStyle(34)
trk_position2.SetMarkerColor(ROOT.kBlack)
trk_position2.SetMarkerSize(2)



graph_good2 = ROOT.TGraph()
graph_good2.SetMarkerStyle(24)
graph_good2.SetMarkerColor(ROOT.kBlue)
graph_good2.SetMarkerSize(3)
graph_bad2 = ROOT.TGraph()
graph_bad2.SetMarkerStyle(26)
graph_bad2.SetMarkerColor(ROOT.kRed)
graph_bad2.SetMarkerSize(3)
goodi = 0
badi = 0
for entry in layer1:
    if criterium(entry[2], vxtime):
        graph_good2.SetPoint(goodi, entry[0] - trk_e0x2, entry[1] - trk_e0y2)
        goodi += 1
    else:
        graph_bad2.SetPoint(badi, entry[0] - trk_e0x2, entry[1] - trk_e0y2)
        badi += 1
        print badi, entry[0] - trk_e0x2, entry[1] - trk_e0y2

# c = ROOT.TCanvas('', '', 800, 800)
# c.SetTicks()
sizeHisto2.Draw()
sizeHisto2.SetAxisRange(-4, 4, "X")
sizeHisto2.SetAxisRange(-4, 4, "Y")
el12 = ROOT.TEllipse(0.,0.,1.,1.);
el22 = ROOT.TEllipse(0.,0.,2.,2.);
el32 = ROOT.TEllipse(0.,0.,3.,3.);
el12.Draw("L SAME")
el22.Draw("SAME")
el32.Draw("SAME")
trk_position2.Draw("P SAME")
if graph_good2.GetN() > 0:
    graph_good2.Draw('P SAME')
if graph_bad2.GetN() > 0:
    graph_bad2.Draw('P SAME')

###########################

c.cd(3)
ROOT.gPad.SetTicks()

sizeHisto3 = ROOT.TH1F("histo", ";x[mm];y[mm]", 7, -4, 3)
sizeHisto3.SetLineColor(ROOT.kWhite   )
sizeHisto3.SetTitle('Layer 2')

trk_position3 = ROOT.TGraph()
trk_position3.SetPoint(1, 0, 0)
trk_position3.SetMarkerStyle(34)
trk_position3.SetMarkerColor(ROOT.kBlack)
trk_position3.SetMarkerSize(2)



graph_good3 = ROOT.TGraph()
graph_good3.SetMarkerStyle(24)
graph_good3.SetMarkerColor(ROOT.kBlue)
graph_good3.SetMarkerSize(3)
graph_bad3 = ROOT.TGraph()
graph_bad3.SetMarkerStyle(26)
graph_bad3.SetMarkerColor(ROOT.kRed)
graph_bad3.SetMarkerSize(3)
goodi = 0
badi = 0
for entry in layer2:
    if criterium(entry[2], vxtime):
        graph_good3.SetPoint(goodi, entry[0] - trk_e0x3, entry[1] - trk_e0y3)
        goodi += 1
    else:
        graph_bad3.SetPoint(badi, entry[0] - trk_e0x3, entry[1] - trk_e0y3)
        badi += 1
        print badi, entry[0] - trk_e0x3, entry[1] - trk_e0y3

# c = ROOT.TCanvas('', '', 800, 800)
# c.SetTicks()
sizeHisto3.Draw()
sizeHisto3.SetAxisRange(-4, 4, "X")
sizeHisto3.SetAxisRange(-4, 4, "Y")
el13 = ROOT.TEllipse(0.,0.,1.,1.);
el23 = ROOT.TEllipse(0.,0.,2.,2.);
el33 = ROOT.TEllipse(0.,0.,3.,3.);
el13.Draw("L SAME")
el23.Draw("SAME")
el33.Draw("SAME")
trk_position3.Draw("P SAME")
if graph_good3.GetN() > 0:
    graph_good3.Draw('P SAME')
if graph_bad3.GetN() > 0:
    graph_bad3.Draw('P SAME')

###########################

c.cd(4)
ROOT.gPad.SetTicks()

sizeHisto4 = ROOT.TH1F("histo", ";x[mm];y[mm]", 7, -4, 3)
sizeHisto4.SetLineColor(ROOT.kWhite   )
sizeHisto4.SetTitle('Layer 3')


trk_position4 = ROOT.TGraph()
trk_position4.SetPoint(1, 0, 0)
trk_position4.SetMarkerStyle(34)
trk_position4.SetMarkerColor(ROOT.kBlack)
trk_position4.SetMarkerSize(2)



graph_good4 = ROOT.TGraph()
graph_good4.SetMarkerStyle(24)
graph_good4.SetMarkerColor(ROOT.kBlue)
graph_good4.SetMarkerSize(3)
graph_bad4 = ROOT.TGraph()
graph_bad4.SetMarkerStyle(26)
graph_bad4.SetMarkerColor(ROOT.kRed)
graph_bad4.SetMarkerSize(3)
goodi = 0
badi = 0
for entry in layer3:
    if criterium(entry[2], vxtime):
        graph_good4.SetPoint(goodi, entry[0] - trk_e0x4, entry[1] - trk_e0y4)
        goodi += 1
    else:
        graph_bad4.SetPoint(badi, entry[0] - trk_e0x4, entry[1] - trk_e0y4)
        badi += 1
        print badi, entry[0] - trk_e0x4, entry[1] - trk_e0y4

# c = ROOT.TCanvas('', '', 800, 800)
# c.SetTicks()
sizeHisto4.Draw()
sizeHisto4.SetAxisRange(-4, 4, "X")
sizeHisto4.SetAxisRange(-4, 4, "Y")
el14 = ROOT.TEllipse(0.,0.,1.,1.);
el24 = ROOT.TEllipse(0.,0.,2.,2.);
el34 = ROOT.TEllipse(0.,0.,3.,3.);
el14.Draw("L SAME")
el24.Draw("SAME")
el34.Draw("SAME")
trk_position4.Draw("P SAME")
if graph_good4.GetN() > 0:
    graph_good4.Draw('P SAME')
if graph_bad4.GetN() > 0:
    graph_bad4.Draw('P SAME')

c.Update();

c.Print("eventDisplay.pdf")
