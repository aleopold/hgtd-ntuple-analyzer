//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jul 25 21:25:04 2018 by ROOT version 6.10/04
// from TChain Data/
//////////////////////////////////////////////////////////

#ifndef selectors/datasel_h
#define selectors/datasel_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector
#include <vector>



class selectors/datasel : public TSelector {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Int_t> m_eventnumber = {fReader, "m_eventnumber"};
   TTreeReaderValue<Int_t> m_runnumber = {fReader, "m_runnumber"};
   TTreeReaderValue<Int_t> m_n_vertices = {fReader, "m_n_vertices"};
   TTreeReaderValue<Double_t> m_vertex_x = {fReader, "m_vertex_x"};
   TTreeReaderValue<Double_t> m_vertex_y = {fReader, "m_vertex_y"};
   TTreeReaderValue<Double_t> m_vertex_z = {fReader, "m_vertex_z"};
   TTreeReaderValue<Double_t> m_truth_vertex_x = {fReader, "m_truth_vertex_x"};
   TTreeReaderValue<Double_t> m_truth_vertex_y = {fReader, "m_truth_vertex_y"};
   TTreeReaderValue<Double_t> m_truth_vertex_z = {fReader, "m_truth_vertex_z"};
   TTreeReaderValue<Double_t> m_truth_vertex_time = {fReader, "m_truth_vertex_time"};
   TTreeReaderArray<float> m_hit_x = {fReader, "m_hit_x"};
   TTreeReaderArray<float> m_hit_y = {fReader, "m_hit_y"};
   TTreeReaderArray<float> m_hit_z = {fReader, "m_hit_z"};
   TTreeReaderArray<double> m_hit_raw_time = {fReader, "m_hit_raw_time"};
   TTreeReaderArray<int> m_hit_has_pulse = {fReader, "m_hit_has_pulse"};
   TTreeReaderArray<double> m_hit_pulse_time = {fReader, "m_hit_pulse_time"};
   TTreeReaderArray<double> m_hit_pulse_resolution = {fReader, "m_hit_pulse_resolution"};
   TTreeReaderArray<int> m_hit_layer = {fReader, "m_hit_layer"};
   TTreeReaderArray<double> m_track_pt = {fReader, "m_track_pt"};
   TTreeReaderArray<double> m_track_e = {fReader, "m_track_e"};
   TTreeReaderArray<double> m_track_eta = {fReader, "m_track_eta"};
   TTreeReaderArray<double> m_track_phi = {fReader, "m_track_phi"};
   TTreeReaderArray<double> m_track_z0 = {fReader, "m_track_z0"};
   TTreeReaderArray<double> m_track_vz = {fReader, "m_track_vz"};
   TTreeReaderArray<int> m_track_is_truthmatched = {fReader, "m_track_is_truthmatched"};
   TTreeReaderArray<unsigned long> m_track_hashid = {fReader, "m_track_hashid"};
   TTreeReaderArray<int> m_track_pdgid = {fReader, "m_track_pdgid"};
   TTreeReaderArray<vector<double>> m_track_extrapolated_pos_l0 = {fReader, "m_track_extrapolated_pos_l0"};
   TTreeReaderArray<vector<double>> m_track_extrapolated_pos_l1 = {fReader, "m_track_extrapolated_pos_l1"};
   TTreeReaderArray<vector<double>> m_track_extrapolated_pos_l2 = {fReader, "m_track_extrapolated_pos_l2"};
   TTreeReaderArray<vector<double>> m_track_extrapolated_pos_l3 = {fReader, "m_track_extrapolated_pos_l3"};
   TTreeReaderArray<vector<double>> m_track_extrapolated_pt_l0 = {fReader, "m_track_extrapolated_pt_l0"};
   TTreeReaderArray<vector<double>> m_track_extrapolated_pt_l1 = {fReader, "m_track_extrapolated_pt_l1"};
   TTreeReaderArray<vector<double>> m_track_extrapolated_pt_l2 = {fReader, "m_track_extrapolated_pt_l2"};
   TTreeReaderArray<vector<double>> m_track_extrapolated_pt_l3 = {fReader, "m_track_extrapolated_pt_l3"};
   TTreeReaderArray<int> m_track_extrapolated_fillfactor_l0 = {fReader, "m_track_extrapolated_fillfactor_l0"};
   TTreeReaderArray<int> m_track_extrapolated_fillfactor_l1 = {fReader, "m_track_extrapolated_fillfactor_l1"};
   TTreeReaderArray<int> m_track_extrapolated_fillfactor_l2 = {fReader, "m_track_extrapolated_fillfactor_l2"};
   TTreeReaderArray<int> m_track_extrapolated_fillfactor_l3 = {fReader, "m_track_extrapolated_fillfactor_l3"};
   TTreeReaderArray<double> m_jet_pt = {fReader, "m_jet_pt"};
   TTreeReaderArray<double> m_jet_e = {fReader, "m_jet_e"};
   TTreeReaderArray<double> m_jet_eta = {fReader, "m_jet_eta"};
   TTreeReaderArray<double> m_jet_phi = {fReader, "m_jet_phi"};
   TTreeReaderArray<int> m_jet_is_hs = {fReader, "m_jet_is_hs"};
   TTreeReaderArray<int> m_jet_is_pu = {fReader, "m_jet_is_pu"};
   TTreeReaderArray<int> m_jet_is_qcd = {fReader, "m_jet_is_qcd"};
   TTreeReaderArray<int> m_jet_is_stoch = {fReader, "m_jet_is_stoch"};
   TTreeReaderArray<vector<unsigned long>> m_jet_ghost_track_hashid = {fReader, "m_jet_ghost_track_hashid"};


   selectors/datasel(TTree * /*tree*/ =0) { }
   virtual ~selectors/datasel() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(selectors/datasel,0);

};

#endif

#ifdef selectors/datasel_cxx
void selectors/datasel::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t selectors/datasel::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef selectors/datasel_cxx
