#!/usr/bin/env python
import os, sys
import subprocess

## split the original input file into several


with open("INPUT_LIST.txt") as f:
    content = f.readlines()

content = [x.strip() for x in content]

# n_jobs = 800
# n_files_in_batch = int(len(content)/n_jobs)
n_jobs = 400
n_files_in_batch = 1
if n_files_in_batch == 0:
    n_files_in_batch = 1

subprocess.call('rm batch/batchconfig*.config', shell=True)
subprocess.call('rm batch/input_*.txt', shell=True)
subprocess.call('rm batch/.batchconfig*.config', shell=True)
subprocess.call('rm batch/.input_*.txt', shell=True)
subprocess.call('rm batch/output/output*.root', shell=True)

file_counter = 0
script = open("batch/.input_"+str(file_counter)+'.txt', 'w')
for i, filename in enumerate(content):
    script.write(filename + '\n')
    if (i%n_files_in_batch == 0):
        script.close()
        with open("globalInput.config", "rt") as fin:
            with open("batch/.batchconfig"+str(file_counter)+".config", "wt") as fout:
                for line in fin:
                    _line = line.replace('INPUT_LIST.txt', 'batch/.input_'+str(file_counter)+'.txt')
                    _line = _line.replace("output.root", "batch/output/output"+str(file_counter)+'.root')
                    fout.write(_line)
        file_counter += 1
        script = open("batch/.input_"+str(file_counter)+'.txt', 'w')

script.close()

## remove previous log files
subprocess.call('rm LOG/*', shell=True)

## submit the jobs with individual config files

working_dir = '/afs/cern.ch/work/a/aleopold/private/hgtd/hgtd-ntuple-analyzer'

# -q 1nw means you are submitting to the 1-week que. Other available queues are:
# 8nm (8 minutes)
# 1nh (1 hour)
# 8nh
# 1nd (1day)
# 2nd
# 1nw (1 week)
# 2nw


for i in range(file_counter):
    configfile = "batch/.batchconfig"+str(i)+".config;"
    # print configfile
    cmd = 'bsub -G u_zp -J ntuple'+str(i)+' -R \"type==SLC6_64&&pool>30000\" -q 8nh -M 2000000 -o LOG/batchlog'+str(i)+'.log  \'source  ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh; cd ' + working_dir + '; source setup.sh; source executeBatch.sh true ' + configfile + '\''
    print cmd
    subprocess.call(cmd, shell=True)
    if i > n_jobs:
       break
#
# bsub -G u_zp -J ntuple -R "type==SLC6_64&&pool>30000" -q 1nw -M 2000000 -o batchlog.log  'cd /afs/cern.ch/work/a/aleopold/private/hgtd/hgtd-ntuple-analyzer; source setup.sh; source executeBatch.sh true batch/batchconfig2.config'
